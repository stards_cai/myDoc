# 登陆后台管理

1. 地址：http://10.7.128.54:15672/

用户密码 admin/admin

![image-20210122185435867](/Users/jack/Library/Application Support/typora-user-images/image-20210122185435867.png)

2. 添加用户admin-dat

   ![image-20210122185539080](/Users/jack/Library/Application Support/typora-user-images/image-20210122185539080.png)

![image-20210122185634822](/Users/jack/Library/Application Support/typora-user-images/image-20210122185634822.png)



3. 添加vhost

   ![image-20210122185738288](/Users/jack/Library/Application Support/typora-user-images/image-20210122185738288.png)

4. 绑定vhost和user

   ![image-20210122185902482](/Users/jack/Library/Application Support/typora-user-images/image-20210122185902482.png)

   ![image-20210122185945414](/Users/jack/Library/Application Support/typora-user-images/image-20210122185945414.png)

   ![image-20210122190019947](/Users/jack/Library/Application Support/typora-user-images/image-20210122190019947.png)

5. 使用刚才创建的admin-dat登陆mq管理页面即可